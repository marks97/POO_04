/* Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse Buyer, la qual és una extenció de la classe User. Aquesta classe compte amb un constructor, en el qual
 * li passem per paràmetres el atributs de la seva classe pare i un atribut d'aquesta.
 *
 * Aquesta subclasse compte amb un dos atributs, els quals són un String accountNumber, el qual li passarem per paràmetres al
 * constructor, i un altre atribut boughtItems, el qual és una linkedList on hi emmagatzarem tots el ítems comprats per el comprador
 * en qüestió.
 *
 */

import java.util.LinkedList;

public class Buyer extends User {

    private String accountNumber;
    private LinkedList<Item> boughtItems;

    public Buyer(String name, String id, String pass, String accountNumber) {   //Constructor de la subclasse
        super(name, id, pass);  //Crida al constructor de la classe pare
        this.accountNumber = accountNumber;
        boughtItems = new LinkedList<>();
    }

    public boolean buy(Item item, Bank b, double quantity) {    //Mètode que fa la funció de comprar un ítem, juntament amb les accions secundaries que això comporta sobre la OnlineStore

        double price = item.getPrice() * quantity;
        if(pay(price * (-1), b)) {
            boughtItems.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    public LinkedList<Item> getBoughtItems() {
        return boughtItems;
    }   //Retorna una LinkedList amb tots el ítmes comprats per un usuari

    private boolean pay(double price, Bank b) {
        return
                b.manageMoney(accountNumber, price);
    }   //Mètode que fa la gestió en el bank quan un Buyer compra un ítem

    public void deposit(double m, Bank b) {
        b.manageMoney(accountNumber, m);
    }   //Mètode per dipositar diners en la conte del banc

    public double getMoney(Bank bank) {
        return bank.getMoney(accountNumber);
    }   //Mètode per obtenir diners de la conte del banc
}
