/* Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse AuctionItem, la qual és filla de la classe Item. El constructor rebrà com a paràmetres
 * els pertanyents a la seva classe pare i els atributs que podem trobar en aquesta mateixa subclasse.
 *
 * En aquesta subclasse tenim dos statics finals; fee i percent, un atribut currentPrice el qual contindrà el preu actual
 * de l'item, una tipus Buyer que contindrà l'útim comprador en fer una licitació i un String deadLine que contingui
 * la data en la qual no és permès fer més apostes.
 *
 */

import java.util.Date;

public class AuctionItem extends Item {

    private static final int fee = 5;
    private static final double percent = 0.05;

    private Double currentPrice;
    private Double bidPrice;
    private Buyer biddler;
    private Date deadLine;
    private Seller seller;

    public AuctionItem(String name, String type, double[] size,
                       double cost, double currentPrice, Date deadLine, Seller seller){

        super (name,type,size,cost);
        this.currentPrice = currentPrice;
        this.deadLine = deadLine;
        this.seller = seller;
        this.bidPrice = currentPrice;
        this.biddler = null;

    }

    public Seller getSeller() {
        return seller;
    }

    public double getPrice(){
        return currentPrice;
    }

    public void setPrice(Double price) {
        this.currentPrice = price;
        this.bidPrice = price;
    }

    public Buyer getBuyer(){
        return biddler;
    }

    public Date getDeadLine(){
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public double calculateProfit () {
        return fee + getCost()*percent;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    //Aquesta funció serveix per fer bids (licitacions)

    public void makeBid(Buyer b, double p, Date dia) {

        if (!frozen(dia)){  //Si encara es permeten bids
            if (p > bidPrice){
                bidPrice = p;
                biddler = b;
                System.out.println("\nCongratulations! Bid completed.");
            } else if (p == bidPrice){
                System.out.println("\nYou have bidded same as the last buyer");
            } else{
                System.out.println("\nYou have bidded less than the last buyer (" + currentPrice + ").");
            }
        } else
            System.out.println("\nTime out, no more bids.");

    }

    //Si el dia que se li passa es mes gran que la deadline llavors retorna frozen com a true

    public boolean frozen (Date date) {
        return
                date.after(deadLine);
    }

}
