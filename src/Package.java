/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Package. En el constructor li passem per paràmetres els atributs d'aquesta pròpia classe
 *
 * Aquesta classe té dos atributs: un int witdth el qual conté la amplada del paquet, i un int height el qual conté la alçada
 *
 * Aquesta classe implementa els mètodes de la interfície taxable
*/

public class Package implements Taxable {

    private final static double MIN_PACKCOST = 0.25;    //valor inventat amb el qual calcularem el preu dels Package segons la mida

    protected int width;
    protected int height;

    public Package(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /*Els mètodes següents partanyen a la interfície Taxable*/
    @Override
    public double getPrice() {  //suposem que el preu d'un paquet depen de la seva alçada i amplada
        return (width*MIN_PACKCOST) + (height*MIN_PACKCOST);
    }

    @Override
    public double getPriceOnlyTax() {   //aquest mètode agafem la funció getPrice i la multipliquem per la taxa (0,21), per tal de trobar l'import de l'iva sobre un Package
        return getPrice()*tax;
    }

    @Override
    public double getPricePlusTax() {   //aquest mètode sumem el preu de l'objecte sense iva amb el preu només amb iva, per tal d'aconseguir el preu final del porducte
        return getPrice() + getPriceOnlyTax();
    }

    @Override
    public double sumTotalTax(Taxable t) {  //aquest mètode suma el preu només amb iva de del Package en el que ens trobem amb un altre classe que implementi Taxable el qual passem par paràmetre
        return getPriceOnlyTax() + t.getPriceOnlyTax();
    }
}

