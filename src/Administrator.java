/* Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse Administrator, la qual és una extenció de la classe User. Aquesta classe compte amb un constructor, en el qual
 * li passem per paràmetres el atributs de la seva classe pare. Aquesta subclasse no compté atributs.
 *
 */
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;

public class Administrator extends User {

    public Administrator(String name, String id, String pass) { //Constructor de la subclasse
        super(name, id, pass);  //crida al constructor de la classe pare
    }

    public boolean expel(User user, LinkedList<User> users) {   //Mètode per eliminar a un user
        if(users.contains(user) && !(user instanceof Administrator)) {
            users.remove(user);
            return true;
        } else
            return false;
    }

    public boolean manageAuction(AuctionItem a, Date date) {    //en el cas de que un biddler no vulgui pagar la seva aposta, llavors poseposem al deadeLine i tornem a posar el preu inicial del producte
        try {
            a.setDeadLine(date);
            a.setPrice(a.getPrice());
            return true;
        } catch (Exception e){
            return false;
        }
    }

    private void printStock(LinkedList<Item> items) {   //Mètode per imprimir tot els ítems disponibles

        System.out.println("\nThis are the stock items\n");
        for(Item item : items) {
            if(item instanceof AuctionItem)
                System.out.println(" - " + item.getName() + "   [" + item.getPrice() + "€]\n");
        }
    }

}
