/* Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse Seller, la qual és una extenció de la classe User. Aquesta classe compte amb un constructor, en el qual
 * li passem per paràmetres el atributs de la seva classe pare i un atribut d'aquesta.
 *
 * Aquesta subclasse compte amb tres atributs, els quals són un String accountNumber, el qual li passarem per paràmetres al
 * constructor, i dos altre atribut soldItems i avaialbleItems, el qual són dues LinkedList, que tenen l'objectiu de emmagatzemar els
 * ítmes venguts i els disponibles d'un venedor.
 */
import java.util.LinkedList;

public class Seller extends User {

    private String accountNumber;
    private LinkedList<AuctionItem> soldItems, availableItems;

    public Seller(String name, String id, String pass, String accountNumber) {  //Constructor de la subcalsse
        super(name, id, pass);  //crida a la classe pare
        this.accountNumber = accountNumber;
        soldItems = new LinkedList<>();
        availableItems = new LinkedList<>();
    }

    public LinkedList<AuctionItem> getAvailableItems() {    //Mètode que retorna la LinkedList amb els ítmes disponibles
        return availableItems;
    }

    public LinkedList<AuctionItem> getSoldItems() { //Mètode que retorna la LinkedList amb els ítmes venguts
        return soldItems;
    }

    public void sell(AuctionItem item, Bank b) {    //Mètode que fa la funció de vendre un ítem

        double price = item.getPrice();
        if(deposit(price, b)) {
            soldItems.add(item);
            availableItems.remove(item);
        }
    }

    public void addAvailableItem(AuctionItem item) {    //Afegir un un ítem a la LinkedList AvailableItem
        availableItems.add(item);
    }

    private boolean deposit(double price, Bank b) { //Posar diners dins de la conte
        return
                b.manageMoney(accountNumber, price);
    }

    public double getMoney(Bank bank) { //treure diners d'una conte
        return bank.getMoney(accountNumber);
    }

}
