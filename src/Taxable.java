/* Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la interfície Taxable, la qual conté un static final tax, amb l'iva, i tres mètodes abstractes, els quals seran implementats
 * en les classes que implementin la interfície.
 *
 */
public interface Taxable {

    static final double tax = 0.21;

    double getPrice();
    double getPriceOnlyTax();
    double getPricePlusTax();
    double sumTotalTax(Taxable t);

}
