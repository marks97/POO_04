import java.util.LinkedList;

public class Bank {

    private LinkedList<String> accounts;
    private LinkedList<Double> ammounts;

    public Bank() {
        accounts = new LinkedList<>();
        ammounts = new LinkedList<>();
    }

    public boolean manageMoney(String account, double ammount) {

        int i = -1;

        for(int x = 0; x < accounts.size(); x++) {
            if(accounts.get(x).equals(account)) {
                i = x;
                break;
            }
        }

        if(i >= 0) {

            double x = ammounts.get(i);

            if( (x + ammount) >= 0 ) {
                ammounts.set(i, x + ammount);
                return true;
            } else
                return false;
        } else {
            if(ammount>=0) {
                accounts.add(account);
                ammounts.add(ammount);
                return true;
            } else
                return false;
        }
    }

    public double getMoney(String account) {

        int i = -1;

        for(int x = 0; x < accounts.size(); x++) {
            if(accounts.get(x).equals(account)) {
                i = x;
                break;
            }
        }

        if(i >= 0) {
            return ammounts.get(i);
        } else
            return 0;

    }

}
