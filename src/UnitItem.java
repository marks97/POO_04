public class UnitItem extends Item {

    private double unitPrice;
    private int quantity;

    public UnitItem(String name, String type, double[] size,
                    double cost, double unitPrice, int quantity) {
        super (name,type,size,cost);
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }
    public double getPrice(){
        return unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double calculateProfit() {
        return getCost() - (unitPrice/quantity);
    }

    public double sell(int q){
        return getCost()*q;
    }

}
