/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse WeightedItem, filla de la classe Item. Al constructor de li passar per paràmetres els
 * atributs de la classe pare i els atributs que podem trobar en aquesta subclasse.
 *
 * Els atributs d'aquesta subclasse són un double pricePerWeight on s'hi emmagatzema el preu de un Item per pes, un
 * double weight en el qual hi tenim el pes que desitja un comprador i un double weightRemaining en el qual hi ha la resta de pes
 *
*/

public class WeightedItem extends Item {

    private double pricePerWeight;
    private double weight;

    public WeightedItem(String name, String type, double[] size,
                        double cost, double pricePerWeight, double weight) {
        super (name,type,size,cost);
        this.pricePerWeight = pricePerWeight;
        this.weight = weight;
    }
    public double getPrice(){
        return pricePerWeight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public  double calculateProfit() {
        return getCost() - pricePerWeight/weight;
    }

    public double sell(double weight){
        return getCost()*weight;
    }

}
