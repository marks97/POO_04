import java.util.Date;

public class Sale implements Comparable<Sale> {

    private Date date;
    private Item item;
    private Buyer buyer;
    public Sale(Date date, Item item, Buyer buyer) {

        this.date = date;
        this.item = item;
        this.buyer = buyer;
    }

    public Date getDate() {
        return date;
    }

    public Item getItem() {
        return item;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    @Override
    public int compareTo(Sale sale) {

        long date1 = this.getDate().getTime();
        long date2 = sale.getDate().getTime();

        if (date1 < date2) {
            return -1;
        }

        if (date1 == date2) {
            return 0;
        }
        return 1;


    }
}
